Ansible Role Docker
=========

Docker installation role

Requirements
------------

Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.

Role Variables
--------------
defaults file for ansible-role-docker:

`docker_version: "5:20.10.14~3-0~ubuntu-focal"`

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: app
      roles:
         - citrulline.ansible_role_docker

License
-------

BSD

Author Information
------------------

Vladislav Pavlov
